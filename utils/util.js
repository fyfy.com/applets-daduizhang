const app = getApp();
const apiUrl = app.appData.url;
const loadingTxt = '拼命加载中...';
const apicontentType = app.appData.contentType;
//request 请求
wxRequest = function(url, params, method) {
  method = method ? method : 'GET';
  return new Promise(function(resolve, reject) {
    wx.request({
      url: apiUrl + url,
      header: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: method,
      data: params,
      success: function(res) {
        resolve(res.data);
      }
    });
  });
} 
//视频列表
getVideoList = function(that) {
  wxRequest('video', {
    chanel_id: that.data.chanel_id,
    option: that.data.option,
  }).then((res) => {
    if (res.code == 0) {
      that.setData({
        listData: res.data,
      });
      if (that.data.chanel_id == 0) {
        wx.setStorage({
          key: "datas",
          data: res.data
        });
      }
    } 
    // 隐藏加载中
    wx.hideLoading();
    wx.stopPullDownRefresh();
  });
}

//收集用户formid 
userFormId = function (formId, openid) {
  wxRequest('form', {
    formid: formId,
    openid: openid
  }, 'POST');
}

//视频详情列表随机20条数据
getRandList = function(that) {
  wxRequest('video/list', {
    id: that.data.id,
  }).then((res) => {
    that.setData({
      listData: res.data, //随机列表
    });
  });
}

//视频详情
getVideoInfo = function(that, txvContext, params) {
  wx.showLoading({
    title: '跳转中...'
  })
  var user = wx.getStorageSync("user");
  var openid = user.openid || 0;
  if (params.share) {
    share = 'ok';
  } else {
    share = '';
  }
  wxRequest('video/info', {
    id: that.data.id,
    openid: openid,
    share: share
  }).then((res) => {
    that.setData({
      infoData: res.data,
      videoShow: true,
    });
    that.txvContext = txvContext.getTxvContext('txv1');
    wx.hideLoading();
  });
}



//获取 美文推荐
getMeiwen = function(that) {
  wxRequest('article').then((res) => {
    that.setData({
      meiwenList: res.data,
    })
  });
}

//提示
openToast = function(msg) {
  wx.showToast({
    title: msg,
    icon: 'success',
    duration: 1000
  });
}
module.exports = {
  getVideoInfo: getVideoInfo,
  getVideoList: getVideoList,
  wxRequest: wxRequest,
  getRandList: getRandList,
  userFormId: userFormId,
  getMeiwen: getMeiwen,
}