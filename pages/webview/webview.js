// pages/webview/webview.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    urls: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    
    switch (options.id){
      case "0":
        this.getConfig('info_left')
      break;
      case "1":
        this.getConfig('info_right')
        break;
      case "2":
        this.getConfig('index_photo')
        break;
      default:break;
    }
   

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },
  getConfig(e){
    let that = this
    wx.request({
      url: app.appData.url +'config?name='+e,  
      success(res) {
        that.setData({
            urls: res.data.data.value
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})