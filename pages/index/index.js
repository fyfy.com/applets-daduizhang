var util = require('../../utils/util.js');
var app = getApp();
var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置
Page({
    data: {
        morePage: 1,
        showMore: false,
        page: 1, //分页当前页
        option: 1, //推荐
        totalPage: 0, //分页总页数
        listData: [], //列表数据
        chanel_id: 0, //频道/栏目
        tabs: [{
                id: 2,
                name: "推荐"
            },
            {
                id: 3,
                name: "美食"
            },
            {
                id: 4,
                name: "情歌"
            },
            {
                id: 5,
                name: "搞笑"
            },
            {
                id: 7,
                name: "社会"
            },
            {
                id: 6,
                name: "祝福"
            },

        ],
        activeIndex: 2,
    },
    onLoad: function(params) {
        var datas = wx.getStorageSync('datas');
        if (datas) {
            this.setData({
                listData: datas,
            });
        } else {
            //视频列表
            getVideoList(this);
        }
        if (params.mod) {
            if (params.mod == 'info') {
                if (params.share) {
                    wx.navigateTo({
                        url: '../info/info?id=' + params.id + '&share=' + params.share
                    })
                } else {
                    wx.navigateTo({
                        url: '../info/info?id=' + params.id
                    })
                }

            }
        }


    },
    //用户下拉重载页面
    onPullDownRefresh: function() {
        getVideoList(this);
    },


    //分享
    onShareAppMessage: function(event) {
        let that = this
        setTimeout(function() {
            that.setData({
                showMore: true,
            })
        }, 1000);
        var info = this.data.listData[0];
        return {
            title: info.title,
            path: 'pages/index/index?mod=info&id=' + info.id + "&share=ok",
            imageUrl: info.thumb
        }
    },

    //跳转到视频详情
    goInfo: function(e) {
        wx.navigateTo({
            url: '../info/info?id=' + e.target.dataset.id
        })
    },

    //导航切换
    tabClick: function(e) {

        var tabid = parseInt(e.target.dataset.id);
        var _chanel_id, _option;
        switch (tabid) {
            case 2:
                _chanel_id = 0;
                _option = 1;
                break;
            case 3:
                _chanel_id = 1;
                _option = 0;
                break;
            case 4:
                _chanel_id = 2;
                _option = 0;
                break;
            case 5:
                _chanel_id = 3;
                _option = 0;
                break;
            case 6:
                _chanel_id = 4;
                _option = 0;
                break;
            case 7:
                _chanel_id = 5;
                _option = 0;
                break;

        }
        this.setData({
            activeIndex: tabid,
            option: _option,
            chanel_id: _chanel_id,
            page: 1,
        });
        getVideoList(this);
        wx.pageScrollTo({
            scrollTop: 0
        })
    },
    //查看更多
    watchMore() {
        wx.reLaunch({
            url: '../index/index'
        })

    },
    //关闭更多
    closeMore() {
        this.setData({
            showMore: false,
        })
    },
    // 收集formId
    orderSign: function(e) {
        var formId = e.detail.formId;
        let user = wx.getStorageSync("user");
        if (formId == 'the formId is a mock one') {
            return false;
        }
        if (formId && user) {
            userFormId(formId, user.openid);
        }
    },

    // 换一换
    refreshPage: function(e) {
        wx.pageScrollTo({
            scrollTop: 0
        });
        this.setData({
            page: 1
        })
        getVideoList(this);
    },

});