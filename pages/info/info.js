var util = require('../../utils/util.js');
var app = getApp();
const txvContext = requirePlugin("tencentvideo"); //加载腾讯视频插件
Page({
  data: {
    showShareMsg:false,
    morePage:1,
    showMore:false,
    page:1,
    chanel_id:0,
    option:1,
    listData: [], //随机列表数据
    infoData: [], //视频详情
    videoShow: false, //显示视频
    id: 0, //当前视频ID
    meiwenList: [], //获取美文列表
    contentHeight: 0, //视频外的高度
  },
  onLoad: function(params) {
    //显示加载中
    var that = this;
    this.setData({
      id: params.id, //2745,//params.id
      videoShow: false, //显示视频
    });
    //创建节点选择器
    var query = wx.createSelectorQuery();
    //设置视频播放器之外的高度
    query.select('.video-player').boundingClientRect()
    query.exec((res) => {
      var videoHeight = res[0].height; // 获取视频高度
      wx.getSystemInfo({
        success: function(res) {
          that.setData({
            contentHeight: res.windowHeight - videoHeight - 5,
          })
        },
      });
    })
    getVideoInfo(this, txvContext, params);
    //视频列表
    getRandList(this);
    getMeiwen(this);
    //分享到群
    this.setTime()
  },

  //分享
  onShareAppMessage: function(res) {
    // getVideoList(this)
   let that = this
    setTimeout(function () {
      that.setData({
        showMore: true,
      })
    }, 1000);
    return {
      title: this.data.infoData.title,
      path: 'pages/index/index?mod=info&id=' + this.data.infoData.id + "&share=ok",
      imageUrl: this.data.infoData.thumb,
    }
  },
  //查看更多
  watchMore(){

    wx.reLaunch({
      url: '../index/index'
    })
   
  },
  //关闭更多
  closeMore(){
    this.setData({
      showMore:false,
    })
  },
  //跳转到详情页面
  goInfo: function(e) {
    wx.redirectTo({
      url: '../info/info?id=' + e.currentTarget.dataset.id,
    })
  },

  // 收集formId
  orderSign: function(e) {
    
    var formId = e.detail.formId;
    var user = wx.getStorageSync("user");
    if (formId == 'the formId is a mock one') {

      return false;
    }
    if (formId && user) {
      userFormId(formId, user.openid);
    }
  },

  //返回首页
  returnIndex: function() {
    wx.reLaunch({
      url: '../index/index'
    })
  },
  //定时器
  setTime(){
    let that = this;
    let times = 
    setInterval(function(){
      that.setData({
        showShareMsg:true
      })
        clearInterval(times);
    },5000)
    setInterval(function () {
      that.setData({
        showShareMsg: false
      })
      clearInterval(times);
    }, 15000)
  }
});